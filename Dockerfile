FROM busybox:1.35.0 AS busybox

FROM scratch

ARG CI_PROJECT_URL
ARG DEFAULT_VAULT_URL
ENV DEFAULT_VAULT_URL="${DEFAULT_VAULT_URL}"
ENV PORT=80
ENV SKIP_SSL=true


# hadolint ignore=DL3048
LABEL Name="vault-secrets-provider" \
      Version="1.0.0" \
      Maintainer="tbc-dev@googlegroups.com" \
      Description="Secrets Provider able to retrieve secrets from a Vault server (Golang-based)" \
      Url=${CI_PROJECT_URL}

COPY --from=busybox /bin/wget   /wget

COPY static       /static
COPY bin/vault_service       /vault_service

HEALTHCHECK --interval=30s --timeout=5s \
    CMD ["/wget", "-Y", "off", "-O", "-", "http://localhost/health" ]

EXPOSE  ${PORT}
CMD ["/vault_service"]
