# Vault Secrets Provider

This project builds a Docker image with an API able to retrieve secrets from a [Vault](https://www.vaultproject.io/) server.

It is aimed at being used in GitLab CI as a [service container](https://docs.gitlab.com/ee/ci/services/)
in order to decouple the image of your jobs and the way of retrieving secrets.

## How to use ?

### Configuring Vault for your CI/CD

Before using this service, you'll have to configure your Vault server, with:

* one or several secrets,
* at least one of the following [Auth Methods](https://www.vaultproject.io/docs/auth) configured with required permissions to access those secrets:
  * [AppRole](https://www.vaultproject.io/docs/auth/approle),
  * [Token](https://www.vaultproject.io/docs/auth/token),
  * or [JWT for GitLab](https://www.vaultproject.io/docs/auth/jwt/oidc_providers#gitlab).

:warning: If using the [AppRole](https://www.vaultproject.io/docs/auth/approle) method, the AppRole used in your CI/CD shall have a **short `token_ttl`**
(let's say 10 minutes) and a **long `secret_id_ttl`** (could be infinite).

This way:

* a new client token will be regenerated every time your CI/CD pipeline is run,
* this token will be valid only for a short period of time,
* but your secret id will remain stable for a longer period.

### API overview

The service exposes one single API to read, create/update or delete a secret from the Vault server.

:warning: the service is smart enough to auto-detect whether the Vault server is configured to use KV Secrets Engine
version 1 (unversioned mode) or version 2 (versioned mode). Thus you don't need to worry about the `data` part
in the resource path or in the response object structure.

#### GET secret endpoint

Read a secret from the Vault server, seamlessly using the right API depending on the detected Key-Value engine version ([version 1](https://www.vaultproject.io/api-docs/secret/kv/kv-v1#read-secret) or [version 2](https://www.vaultproject.io/api-docs/secret/kv/kv-v2#read-secret-version)).

```curl
GET /api​/secrets​/{secret_path}
```

##### Parameters

| Name                             | description                            |
| -------------------------------- | -------------------------------------- |
| `secret_path` (_path parameter_) | this is your secret location in the Vault server |
| `field` (_query parameter_)      | parameter to access a single basic field from the secret JSON payload |
| `b64decode` (_query parameter_)  | paramater to decode a single basic field in b64 from the secret |

##### Example

Let's suppose your have a secret stored under `/b7ecb6ebabc231/my-backend/prod` location in Vault with JSON payload:

```json
{
  "token": "ŧ0k3N",
  "secret": "$€cr€T",
  "mysql": {
    "user": "root",
    "password": "p@s5w0rd"
  },
  "config": "ewogICJraW5kIjogIkNvbmZpZyIsCiAgImFwaVZlcnNpb24iOiAidjEiLAogICJwcmVmZXJlbmNlcyI6IHt9LAp9Cg=="
}
```

Then you may retrieve:

* the token calling `GET http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/my-backend/prod?field=token`
* the MySql password calling `GET http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/my-backend/prod?field=mysql.password`
* the config b64 decoded calling `GET http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/my-backend/prod?field=config&b64decode`

#### PUT secret endpoint

Create/Update a secret into the Vault server, seamlessly using the right API depending on the detected Key-Value engine version ([version 1](https://www.vaultproject.io/api-docs/secret/kv/kv-v1#create-update-secret) or [version 2](https://www.vaultproject.io/api-docs/secret/kv/kv-v2#create-update-secret)).

```curl
PUT /api​/secrets​/{secret_path}
 -d '{"token": "@n0ther_ŧ0k3N", "secret": "n€w_$€cr€T"}' 
 -H "Content-Type: application/json"
```

:warning: depending on the Key-Value engine version the Vault server is using, this might either overwrite the secret (v1) or create a new version (v2).
When using version 2, you don't need to embed your secret payload into a `data` field - this is done automatically for you.

##### Parameters

| Name                             | description                            |
| -------------------------------- | -------------------------------------- |
| `secret_path` (_path parameter_) | this is your secret location in the Vault server |

##### Example

Create/update a secret under `/b7ecb6ebabc231/my-backend/review` location in Vault with JSON payload:

```curl
curl -X PUT http://localhost:8080/api/secret/b7ecb6ebabc231/my-backend/review \
 -d '{"token": "@n0ther_ŧ0k3N", "secret": "n€w_$€cr€T"}' 
 -H "Content-Type: application/json"
```

```json
{
  "created_time":"2021-11-10T10:40:49.286084835Z",
  "deletion_time":"",
  "destroyed":false,
  "version":1
}
```

#### DELETE secret endpoint

Delete a secret from the Vault server, seamlessly using the right API depending on the detected Key-Value engine version ([version 1](https://www.vaultproject.io/api-docs/secret/kv/kv-v1#delete-secret) or [version 2](https://www.vaultproject.io/api-docs/secret/kv/kv-v2#delete-latest-version-of-secret)).

```curl
DELETE /api​/secrets​/{secret_path}
```

##### Parameters

| Name                             | description                            |
| -------------------------------- | -------------------------------------- |
| `secret_path` (_path parameter_) | this is your secret location in the Vault server |

##### Example

Delete a secret stored under `/b7ecb6ebabc231/my-backend/review` location in Vault with JSON payload:

```curl
curl -X DELETE http://localhost:8080/api/secret/b7ecb6ebabc231/my-backend/review
```

### Required image parameters

The tool requires the following environment variables to be set (as GitLab CI secret variables):

| Name              | description                            | default value     |
| ----------------- | -------------------------------------- | ----------------- |
| `VAULT_BASE_URL`  | The Vault server base API url          | _none_ |
| `VAULT_NAMESPACE` | The Vault [Namespace](https://www.vaultproject.io/api-docs#namespaces) to retrieve secrets into | _none_ |
| `VAULT_BASE_AUTH_APPROLE_PATH`| The base [AppRole authentication](https://www.vaultproject.io/api-docs/auth/approle) API path | `/auth/approle` |
| `VAULT_BASE_AUTH_JWT_PATH`    | The base [JWT/OIDC authentication](https://www.vaultproject.io/api-docs/auth/jwt) API path | `/auth/jwt` |
| `VAULT_BASE_KV_SECRETS_PATH`  | The base [Key/Value secrets](https://www.vaultproject.io/api-docs/secret/kv/kv-v1) API path | `/secret` |
| `VAULT_ROLE_ID`   | The [AppRole](https://www.vaultproject.io/docs/auth/approle) RoleID <br/>_Required for the [AppRole](https://www.vaultproject.io/docs/auth/approle) Auth Method_ | _none_ |
| `VAULT_SECRET_ID` | The [AppRole](https://www.vaultproject.io/docs/auth/approle) SecretID <br/>_Required for the [AppRole](https://www.vaultproject.io/docs/auth/approle) Auth Method_ | _none_ |
| `VAULT_TOKEN`     | The authentication token <br/>_Required for the [Token](https://www.vaultproject.io/docs/auth/token) Auth Method_ | _none_ |
| `VAULT_JWT_TOKEN` | The signed [JSON Web Token](https://en.wikipedia.org/wiki/JSON_Web_Token) to login <br/>_Required for the [JWT/OIDC](https://www.vaultproject.io/docs/auth/jwt) Auth Method_ | `$CI_JOB_JWT` |
| `VAULT_JWT_ROLE`  | Name of the role against which the login is being attempted  <br/>_Required for the [JWT/OIDC](https://www.vaultproject.io/docs/auth/jwt) Auth Method_ | `default_role` |

If no authentication parameter is set, the image will emit an error log at startup.

### Use in GitLab CI

Finally, the Docker image can be used in your GitLab CI files as follows:

```yaml
variables:
  # variables have to be explicitly declared in the YAML to be exported to the service
  VAULT_BASE_URL: "https://vault.secrets.acme.host/v1"
  VAULT_ROLE_ID: "$VAULT_ROLE_ID"
  VAULT_SECRET_ID: "$VAULT_SECRET_ID"

deploy-job:
  image: my-deploy-tool:latest
  services: 
    # add Vault Secrets Provider as a service
    # requires that VAULT_ROLE_ID and VAULT_SECRET_ID are declared as secret variables
    - name: $CI_REGISTRY/to-be-continuous/tools/vault-secrets-provider:master
      alias: vault-secrets-provider
  before-script:
    # retrieve some token from Vault server
    - my_token=$(curl -s -S -f "http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/my-backend/prod?field=token")
    # then login
    - my-deploy-tool login --token $my_token
  script:
    # deploy (pseudo code)
    - my-deploy-tool deploy --other --args
```

Depending on what is available in your docker image, you may request the service using either `curl` or `wget`:

* `curl -s -S -f "http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/my-backend/prod?field=token"`
* `wget -O - "http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/my-backend/prod?field=token"`

### How to test & debug

You might want to test/debug whether you have the right secret ID, role ID, secret path, secret key or so.

You may either use a remote Vault server, or run one locally using the 
[Vault Docker image](https://hub.docker.com/_/vault) in dev mode on port 8200, specifying the root token value:

```bash
docker run -d -p 8200:8200 --env VAULT_DEV_ROOT_TOKEN_ID=dev-token vault 
```

Then run the Vault Secrets Provider Docker image:

```bash
# adapt the Vault server URL to the one you chose
# use this value if using a local Vault server
export VAULT_BASE_URL=http://host.docker.internal:8200/v1
# maybe use other authentication variables (depending on your Auth Method)
export VAULT_TOKEN=dev-token
docker run -it -p 80:80 --env VAULT_BASE_URL --env VAULT_TOKEN registry.gitlab.com/to-be-continuous/tools/vault-secrets-provider:master
```

Then you may simply debug the API using `curl` with the `-w "\n>> status: %{http_code}\n"` option. Examples:

```bash
$ curl -s -w "\n>> status: %{http_code}\n" "http://localhost/api/secrets/b7ecb6ebabc231/my-backend/prod?field=token"
ŧ0k3N
>> status: 200

$ curl -s -w "\n>> status: %{http_code}\n" "http://localhost/api/secrets/no/such/path?field=token"
Vault server error on GET http://host.docker.internal:8200/v1/secret/data/no/such/path > 
>> status: 404

$ curl -s -w "\n>> status: %{http_code}\n" "http://localhost/api/secrets/b7ecb6ebabc231/my-backend/prod?field=no.such.field"
Invalid key path 'data.no.such.field' > No such object: 'data.no.such.field'
>> status 400
```

_et voilà_ :)
