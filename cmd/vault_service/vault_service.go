/*
 * Copyright (C) 2021 Orange & contributors
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 *
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */

package main

import (
	"crypto/tls"
	"fmt"
	"log"
	"net/http"

	. "tbc.orange.com/vault-secrets-provider/cmd/vault_service/internal"
)

func main() {
	skipSsl := EnvBool("SKIP_SSL").Or(false)
	if skipSsl {
		log.Println("Disabling SSL verification ($SKIP_SSL is set)...")
		// nolint
		http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	}

	port := EnvInt("PORT").Or(8080)
	log.Printf("Launching service on port %d\n", port)

	DumpVaultCfg()

	// health endpoint
	http.HandleFunc("/health", Health)

	// API secrets endpoint
	http.HandleFunc("/api/secrets/", SecretsEndpoint)

	// serve static resources
	fs := http.FileServer(http.Dir("./static"))
	http.Handle("/", fs)

	// nolint
	http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
}
