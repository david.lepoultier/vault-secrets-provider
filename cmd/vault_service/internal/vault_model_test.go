/*
 * Copyright (C) 2021 Orange & contributors
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 *
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */

package internal

import (
	"bytes"
	"encoding/json"
	"testing"
)

func Test_approle_login_marshalling(t *testing.T) {
	expected := []byte(`{"role_id":"59d6d1ca-47bb-4e7e-a40b-8be3bc5a0ba8","secret_id":"84896a0c-1347-aa90-a4f6-aca8b7558780"}`)
	body := AppRoleLoginBody{
		RoleId:   "59d6d1ca-47bb-4e7e-a40b-8be3bc5a0ba8",
		SecretId: "84896a0c-1347-aa90-a4f6-aca8b7558780",
	}
	if marshalled, err := json.Marshal(body); err != nil {
		t.Fatalf("Error while marshalling message: %v", err)
	} else if !bytes.Equal(expected, marshalled) {
		t.Fatalf("Encoding error\nExpected:\n%s\nGot:\n%s", string(expected), string(marshalled))
	}
}
func Test_jwt_login_with_role_marshalling(t *testing.T) {
	expected := []byte(`{"role":"dev-role","jwt":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"}`)
	body := JwtLoginBody{
		Role:  "dev-role",
		Token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9",
	}
	if marshalled, err := json.Marshal(body); err != nil {
		t.Fatalf("Error while marshalling message: %v", err)
	} else if !bytes.Equal(expected, marshalled) {
		t.Fatalf("Encoding error\nExpected:\n%s\nGot:\n%s", string(expected), string(marshalled))
	}
}
func Test_jwt_login_without_role_marshalling(t *testing.T) {
	expected := []byte(`{"jwt":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"}`)
	body := JwtLoginBody{
		Token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9",
	}
	if marshalled, err := json.Marshal(body); err != nil {
		t.Fatalf("Error while marshalling message: %v", err)
	} else if !bytes.Equal(expected, marshalled) {
		t.Fatalf("Encoding error\nExpected:\n%s\nGot:\n%s", string(expected), string(marshalled))
	}
}
func Test_login_response_unmarshalling(t *testing.T) {
	response := `{
		"auth": {
		  "renewable": true,
		  "lease_duration": 1200,
		  "metadata": null,
		  "token_policies": ["default"],
		  "accessor": "fd6c9a00-d2dc-3b11-0be5-af7ae0e1d374",
		  "client_token": "5b1a0318-679c-9c45-e5c6-d1b9a9035d49"
		},
		"warnings": null,
		"wrap_info": null,
		"data": null,
		"lease_duration": 0,
		"renewable": false,
		"lease_id": "12345"
	  }`
	var loginResp LoginResponse
	if err := json.Unmarshal([]byte(response), &loginResp); err != nil {
		t.Fatalf("Error while unmarshalling message: %v", err)
	} else if loginResp.Auth.ClientToken != "5b1a0318-679c-9c45-e5c6-d1b9a9035d49" {
		t.Fatalf("Decoding error\nExpected:\n%s\nGot:\n%s", "5b1a0318-679c-9c45-e5c6-d1b9a9035d49", loginResp.Auth.ClientToken)
	}
}
func Test_get_secret_response_unmarshalling(t *testing.T) {
	response := `{
		"auth": null,
		"data": {
		  "foo": "bar",
		  "ttl": "1h"
		},
		"lease_duration": 3600,
		"lease_id": "12345",
		"renewable": false
	  }`
	var getSecretResp GetSecretResponse
	if err := json.Unmarshal([]byte(response), &getSecretResp); err != nil {
		t.Fatalf("Error while unmarshalling message: %v", err)
	} else if getSecretResp.Data["foo"] != "bar" {
		t.Fatalf("Decoding error\nExpected:\n%s\nGot:\n%s", "bar", getSecretResp.Data["foo"])
	}
}

func Test_update_secret_response_unmarshalling(t *testing.T) {
	response := `{
		"auth": null,
		"data": {
		  "created_time": "2021-11-10T10:11:12.400489065Z",
		  "deletion_time": "",
			"destroyed": false,
			"version": 1
		},
		"lease_duration": 0,
		"lease_id": "",
		"renewable": false
	  }`
	var updateSecretResp UpdateSecretResponse
	if err := json.Unmarshal([]byte(response), &updateSecretResp); err != nil {
		t.Fatalf("Error while unmarshalling message: %v", err)
	} else if updateSecretResp.Data["created_time"] != "2021-11-10T10:11:12.400489065Z" {
		t.Fatalf("Decoding error\nExpected:\n%s\nGot:\n%s", "2021-11-10T10:11:12.400489065Z", updateSecretResp.Data["created_time"])
	}
}

func Test_get_error_unmarshalling_simple(t *testing.T) {
	response := `{
		"errors": ["invalid role ID"]
	  }`
	var errorResp ErrorResponse
	if err := json.Unmarshal([]byte(response), &errorResp); err != nil {
		t.Fatalf("Error while unmarshalling message: %v", err)
	} else if errorResp.Error() != "invalid role ID" {
		t.Fatalf("Decoding error\nExpected:\n%s\nGot:\n%s", "invalid role ID", errorResp.Error())
	}
}

func Test_get_error_unmarshalling_multiline(t *testing.T) {
	response := `{
		"errors": ["1 error occurred:\n\t* permission denied\n\n"]
	  }`
	var errorResp ErrorResponse
	if err := json.Unmarshal([]byte(response), &errorResp); err != nil {
		t.Fatalf("Error while unmarshalling message: %v", err)
	} else if errorResp.Error() != "permission denied" {
		t.Fatalf("Decoding error\nExpected:\n%s\nGot:\n%s", "permission denied", errorResp.Error())
	}
}

func Test_get_error_unmarshalling_complex(t *testing.T) {
	response := `{
		"errors": ["invalid role ID", "2 errors occurred:\n\t* permission denied\n\t* some other thing\n\n"]
	  }`
	var errorResp ErrorResponse
	if err := json.Unmarshal([]byte(response), &errorResp); err != nil {
		t.Fatalf("Error while unmarshalling message: %v", err)
	} else if errorResp.Error() != "invalid role ID, permission denied, some other thing" {
		t.Fatalf("Decoding error\nExpected:\n%s\nGot:\n%s", "invalid role ID, permission denied, some other thing", errorResp.Error())
	}
}
