/*
 * Copyright (C) 2021 Orange & contributors
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 *
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */

package internal

import (
	"testing"
)

func Test_get_sub_root(t *testing.T) {
	obj := map[string]interface{}{
		"key": "value",
		"map": map[string]interface{}{
			"k1": "111",
			"k2": "222",
			"k3": "333",
		},
	}
	path := "."
	expected := `{"key":"value","map":{"k1":"111","k2":"222","k3":"333"}}`
	if sub, err := getObj(obj, path, false); err != nil {
		t.Fatalf("Error retrieving sub object: %v", err)
	} else if sub != expected {
		t.Fatalf("Assert error\nExpected:\n%s\nGot:\n%s", string(expected), string(sub))
	}
}

func Test_get_sub_key(t *testing.T) {
	obj := map[string]interface{}{
		"key": "value",
		"map": map[string]interface{}{
			"k1": "111",
			"k2": "222",
			"k3": "333",
		},
	}
	path := "key"
	expected := `value`
	if sub, err := getObj(obj, path, false); err != nil {
		t.Fatalf("Error retrieving sub object: %v", err)
	} else if sub != expected {
		t.Fatalf("Assert error\nExpected:\n%s\nGot:\n%s", string(expected), string(sub))
	}
}

func Test_get_sub_map(t *testing.T) {
	obj := map[string]interface{}{
		"key": "value",
		"map": map[string]interface{}{
			"k1": "111",
			"k2": "222",
			"k3": "333",
		},
	}
	path := "map"
	expected := `{"k1":"111","k2":"222","k3":"333"}`
	if sub, err := getObj(obj, path, false); err != nil {
		t.Fatalf("Error retrieving sub object: %v", err)
	} else if sub != expected {
		t.Fatalf("Assert error\nExpected:\n%s\nGot:\n%s", string(expected), string(sub))
	}
}

func Test_get_sub_sub_key(t *testing.T) {
	obj := map[string]interface{}{
		"key": "value",
		"map": map[string]interface{}{
			"k1": "111",
			"k2": "222",
			"k3": "333",
		},
	}
	path := "map.k2"
	expected := "222"
	if sub, err := getObj(obj, path, false); err != nil {
		t.Fatalf("Error retrieving sub object: %v", err)
	} else if sub != expected {
		t.Fatalf("Assert error\nExpected:\n%s\nGot:\n%s", string(expected), string(sub))
	}
}

func Test_get_sub_sub_key_b64decode(t *testing.T) {
	obj := map[string]interface{}{
		"key": "value",
		"map": map[string]interface{}{
			"k1": "111",
			"k2": "MjIyCg==", // 222 in b64
			"k3": "333",
		},
	}
	path := "map.k2"
	expected := "222"
	if sub, err := getObj(obj, path, true); err != nil {
		t.Fatalf("Error retrieving sub object b64decode: %v", err)
	} else if sub != expected {
		t.Fatalf("Assert error\nExpected:\n%s\nGot:\n%s", string(expected), string(sub))
	}
}

func Test_get_sub_key_should_fail(t *testing.T) {
	obj := map[string]interface{}{
		"key": "value",
		"map": map[string]interface{}{
			"k1": "111",
			"k2": "222",
			"k3": "333",
		},
	}
	path := "nosuchkey"
	if _, err := getObj(obj, path, false); err == nil {
		t.Fatalf("Error was expected")
	}
}

func Test_get_sub_sub_key_should_fail(t *testing.T) {
	obj := map[string]interface{}{
		"key": "value",
		"map": map[string]interface{}{
			"k1": "111",
			"k2": "222",
			"k3": "333",
		},
	}
	path := "map.nosuchkey"
	if _, err := getObj(obj, path, false); err == nil {
		t.Fatalf("Error was expected")
	}
}

func Test_get_sub_sub_key_b64decode_should_fail(t *testing.T) {
	obj := map[string]interface{}{
		"key": "value",
		"map": map[string]interface{}{
			"k1": "111",
			"k2": "222",
			"k3": "333",
		},
	}
	path := "map.k2"
	if _, err := getObj(obj, path, true); err == nil {
		t.Fatalf("Error was expected")
	}
}
