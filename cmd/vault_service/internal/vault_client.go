/*
 * Copyright (C) 2021 Orange & contributors
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 *
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */

package internal

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

type StatusError struct {
	Code    int
	Message string
	Err     error
}

func TrimSuffix(s, suffix string) string {
	str := strings.HasSuffix(s, suffix)
	if str {
		s = s[:len(s)-len(suffix)]
	}
	return s
}

func (se StatusError) Error() string {
	if se.Err != nil {
		return se.Message + " > " + se.Err.Error()
	}
	return se.Message
}

func (se StatusError) Status() int {
	if se.Code == 0 {
		if serr, ok := se.Err.(StatusError); ok {
			// if parent error is of type StatusError: inherit
			return serr.Code
		} else {
			// else: internal error
			return http.StatusInternalServerError
		}
	} else {
		return se.Code
	}
}

var path2secret map[string]map[string]interface{} = make(map[string]map[string]interface{})

var (
	vaultBaseUrl             = EnvStr("VAULT_BASE_URL").Or(os.Getenv("DEFAULT_VAULT_URL"))
	vaultBaseAuthJwtPath     = EnvStr("VAULT_BASE_AUTH_JWT_PATH").Or("/auth/jwt")
	vaultBaseAuthApprolePath = EnvStr("VAULT_BASE_AUTH_APPROLE_PATH").Or("/auth/approle")
	vaultBaseSecretsKvPath   = EnvStr("VAULT_BASE_KV_SECRETS_PATH").Or("/secret")
	vaultNamespace           = os.Getenv("VAULT_NAMESPACE")
	vaultJwtToken            = EnvStr("VAULT_JWT_TOKEN").Or(os.Getenv("CI_JOB_JWT"))
	vaultJwtRole             = os.Getenv("VAULT_JWT_ROLE")
	vaultRoleId              = os.Getenv("VAULT_ROLE_ID")
	vaultSecretId            = os.Getenv("VAULT_SECRET_ID")
	clientToken              = os.Getenv("VAULT_TOKEN")
	expirationTimeSec        = time.Now().Unix() + 86400 // 24 hours from start time
	kvEngineVersion          = 0
)

func DumpVaultCfg() {
	log.Printf("Vault server base url: '%s'\n", vaultBaseUrl)
	if len(clientToken) > 0 {
		log.Printf("Vault Auth method: Token (provided)\n")
	} else if len(vaultRoleId) > 0 && len(vaultSecretId) > 0 {
		log.Printf("Vault Auth method: AppRole (path: '%s')\n", vaultBaseAuthApprolePath)
	} else if len(vaultJwtToken) > 0 {
		log.Printf("Vault Auth method: JWT (path: '%s')\n", vaultBaseAuthJwtPath)
	} else {
		log.Printf("WARN: no Vault Auth method configured\n")
	}
	log.Printf("Vault namespace: '%s'\n", vaultNamespace)
	log.Printf("Vault base secrets/KV path: '%s'\n", vaultBaseSecretsKvPath)
}

/**
 * Determines whether the token is valid or not
 */
func hasValidToken() bool {
	return len(clientToken) > 0 && time.Now().Unix() < (expirationTimeSec-1)
}

/**
 * This function returns a valid authentication token, caching the value (thus the login request to the Vault server is executed only once)
 */
func getToken() (string, error) {
	if !hasValidToken() {
		if auth, err := doLogin(); err != nil {
			return "", err
		} else {
			// save in cache
			log.Printf("... client token obtained (expires in %ds)\n", auth.LeaseDuration)
			clientToken = auth.ClientToken
			expirationTimeSec = time.Now().Unix() + int64(auth.LeaseDuration)
		}
	} else {
		log.Printf("... reuse client token from cache\n")
	}
	// retrieve from cache
	return clientToken, nil
}

/**
 * This function requests the Vault server endpoint to perform the login (depending on the used Auth method)
 * - AppRole Auth method: https://www.vaultproject.io/api-docs/auth/approle#login-with-approle
 * - JWT/OIDC Auth method: https://www.vaultproject.io/api-docs/auth/jwt#jwt-login
 */
func doLogin() (*Authentication, error) {
	var loginObj interface{}
	var loginUrl string

	if len(vaultRoleId) > 0 && len(vaultSecretId) > 0 {
		loginUrl = fmt.Sprintf("%s%s/login", vaultBaseUrl, vaultBaseAuthApprolePath)
		log.Printf("... login with AppRole (POST %s)\n", loginUrl)
		loginObj = AppRoleLoginBody{
			RoleId:   vaultRoleId,
			SecretId: vaultSecretId,
		}
	} else if len(vaultJwtToken) > 0 {
		loginUrl = fmt.Sprintf("%s%s/login", vaultBaseUrl, vaultBaseAuthJwtPath)
		log.Printf("... login with JWT (POST %s)\n", loginUrl)
		loginObj = JwtLoginBody{
			Role:  vaultJwtRole,
			Token: vaultJwtToken,
		}
	} else {
		return nil, StatusError{
			Code:    http.StatusServiceUnavailable,
			Message: "No credential provided",
		}
	}

	if loginBody, err := json.Marshal(loginObj); err != nil {
		return nil, StatusError{
			Code:    http.StatusInternalServerError,
			Message: "Failed marshalling request body",
			Err:     err,
		}
	} else if request, err := http.NewRequest(http.MethodPost, loginUrl, bytes.NewBuffer(loginBody)); err != nil {
		// create request error: propagate
		return nil, StatusError{
			Code:    http.StatusInternalServerError,
			Message: "Failed creating request",
			Err:     err,
		}
	} else {
		request.Header.Set("Content-Type", "application/json")
		if len(vaultNamespace) > 0 {
			request.Header.Set(NamespaceHeader, vaultNamespace)
		}

		client := &http.Client{}
		if resp, err := client.Do(request); err != nil {
			// request error: propagate
			return nil, StatusError{
				Code:    http.StatusInternalServerError,
				Message: "Failed sending request",
				Err:     err,
			}
		} else if resp.StatusCode != http.StatusOK {
			// http error: decode error body and throw error
			defer resp.Body.Close()
			var errResp ErrorResponse
			// nolint
			json.NewDecoder(resp.Body).Decode(&errResp)

			return nil, StatusError{
				Code:    resp.StatusCode,
				Message: fmt.Sprintf("Vault server error on POST %s", loginUrl),
				Err:     errResp,
			}
		} else {
			// OK: decode response
			defer resp.Body.Close()
			var loginResp LoginResponse
			if err := json.NewDecoder(resp.Body).Decode(&loginResp); err != nil {
				// JSON unmarshall error: propagate
				return nil, StatusError{
					Code:    http.StatusInternalServerError,
					Message: "Failed unmarshalling response body",
					Err:     err,
				}
			}
			log.Println("... login success")
			return loginResp.Auth, nil
		}
	}
}

/**
 * This function returns the KV engine version, caching the value (thus the request to the Vault endpoint is executed only once)
 */
func getKvEngineVersion(secretPath string) (int, error) {
	if kvEngineVersion == 0 {
		// need to determine actual KV engine version on server
		if version, err := doGetKvEngineVersion(secretPath); err != nil {
			return 0, err
		} else {
			kvEngineVersion = version
		}
	}
	return kvEngineVersion, nil
}

/**
 * This function requests the Vault server endpoint to read the KV engine version
 * https://www.vaultproject.io/api-docs/system/internal-ui-mounts
 */
func doGetKvEngineVersion(secretPath string) (int, error) {
	url := fmt.Sprintf("%s/sys/internal/ui/mounts%s/%s", vaultBaseUrl, vaultBaseSecretsKvPath, secretPath)

	if request, err := http.NewRequest(http.MethodGet, url, nil); err != nil {
		// create request error: propagate
		return 0, StatusError{
			Code:    http.StatusInternalServerError,
			Message: "Failed creating request",
			Err:     err,
		}
	} else if token, err := getToken(); err != nil {
		// login error: propagate
		return 0, StatusError{
			Message: "Login failed",
			Err:     err,
		}
	} else {
		request.Header.Set("Content-Type", "application/json")
		request.Header.Set(ClientTokenHeader, token)
		if len(vaultNamespace) > 0 {
			request.Header.Set(NamespaceHeader, vaultNamespace)
		}

		client := &http.Client{}
		log.Printf("... retrieve KV engine version (GET %s)\n", url)
		if resp, err := client.Do(request); err != nil {
			// request error: propagate
			return 0, StatusError{
				Code:    http.StatusInternalServerError,
				Message: "Failed sending request",
				Err:     err,
			}
		} else if resp.StatusCode != http.StatusOK {
			// http error: decode error body and throw error
			defer resp.Body.Close()
			var errResp ErrorResponse
			// nolint
			json.NewDecoder(resp.Body).Decode(&errResp)

			return 0, StatusError{
				Code:    resp.StatusCode,
				Message: fmt.Sprintf("Vault server error on GET %s", url),
				Err:     errResp,
			}
		} else {
			// OK: decode response
			defer resp.Body.Close()
			var getUiMountsResp GetSecretResponse
			if err := json.NewDecoder(resp.Body).Decode(&getUiMountsResp); err != nil {
				// JSON unmarshall error: propagate
				return 0, StatusError{
					Code:    http.StatusInternalServerError,
					Message: "Failed unmarshalling response body",
					Err:     err,
				}
			}
			// log.Printf("... ui mounts config retrieved: %v\n", getUiMountsResp.Data)
			kvVersion := 1
			if optionsMap, ok := getUiMountsResp.Data["options"].(map[string]interface{}); ok {
				if optionsMap["version"] == "2" {
					kvVersion = 2
				}
			}
			log.Printf("... KV engine version is: %d\n", kvVersion)
			return kvVersion, nil
		}
	}
}

/**
 * This function requests the Vault server endpoint to read a (latest version of a) secret.
 * It manages seamlessly the KV engine version
 * - version 1: https://www.vaultproject.io/api-docs/secret/kv/kv-v1#read-secret
 * - version 2: https://www.vaultproject.io/api-docs/secret/kv/kv-v2#read-secret-version
 */
func doGetSecret(secretPath string) (map[string]interface{}, error) {
	// 1: determine KV engine version
	if version, err := getKvEngineVersion(secretPath); err != nil {
		return nil, StatusError{
			Code:    http.StatusInternalServerError,
			Message: "Failed retrieving KV engine version",
			Err:     err,
		}
	} else {
		if version == 2 {
			// append "data/"
			secretPath = "data/" + secretPath
		}
	}

	url := fmt.Sprintf("%s%s/%s", vaultBaseUrl, vaultBaseSecretsKvPath, secretPath)

	if request, err := http.NewRequest(http.MethodGet, url, nil); err != nil {
		// create request error: propagate
		return nil, StatusError{
			Code:    http.StatusInternalServerError,
			Message: "Failed creating request",
			Err:     err,
		}
	} else if token, err := getToken(); err != nil {
		// login error: propagate
		return nil, StatusError{
			Message: "Login failed",
			Err:     err,
		}
	} else {
		request.Header.Set("Content-Type", "application/json")
		request.Header.Set(ClientTokenHeader, token)
		if len(vaultNamespace) > 0 {
			request.Header.Set(NamespaceHeader, vaultNamespace)
		}

		log.Printf("... retrieve secret '%s' (GET %s)\n", secretPath, url)
		client := &http.Client{}
		if resp, err := client.Do(request); err != nil {
			// request error: propagate
			return nil, StatusError{
				Code:    http.StatusInternalServerError,
				Message: "Failed sending request",
				Err:     err,
			}
		} else if resp.StatusCode != http.StatusOK {
			// http error: decode error body and throw error
			defer resp.Body.Close()
			var errResp ErrorResponse
			// nolint
			json.NewDecoder(resp.Body).Decode(&errResp)

			return nil, StatusError{
				Code:    resp.StatusCode,
				Message: fmt.Sprintf("Vault server error on GET %s", url),
				Err:     errResp,
			}
		} else {
			// OK: decode response
			defer resp.Body.Close()
			var secret GetSecretResponse
			if err := json.NewDecoder(resp.Body).Decode(&secret); err != nil {
				// JSON unmarshall error: propagate
				return nil, StatusError{
					Code:    http.StatusInternalServerError,
					Message: "Failed unmarshalling response body",
					Err:     err,
				}
			}
			// log.Printf("... secret %s retrieved: %s\n", secretPath, secret.Data)
			return secret.Data, nil
		}
	}
}

/**
 * This function returns the given secret field, caching the JSON value (thus the request to read the JSON secret is executed only once,
 * even if subsequent calls are made to read other fields of the same secret)
 */
func getSecret(secretPath string, keyPath string, b64Decode bool) (string, error) {
	log.Printf("Get secret '%s' (%s)\n", secretPath, keyPath)

	secretMap := path2secret[secretPath]
	if secretMap == nil {
		log.Printf("... secret '%s' not in cache: request\n", secretPath)
		var err error
		secretMap, err = doGetSecret(secretPath)
		if err != nil {
			return "", err
		}
		// save in cache
		path2secret[secretPath] = secretMap
	} else {
		log.Printf("... secret '%s' retrieved from cache\n", secretPath)
	}

	// then return sub object (as string or JSON)
	if kvEngineVersion == 2 {
		if strings.HasPrefix(keyPath, ".") {
			keyPath = "data" + keyPath
		} else {
			keyPath = "data." + keyPath
		}
	}

	log.Printf("... extract '%s' from secret\n", keyPath)
	if secret, err := getObj(secretMap, keyPath, b64Decode); err != nil {
		return "", StatusError{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("Invalid key path '%s'", keyPath),
			Err:     err,
		}
	} else {
		return secret, nil
	}
}

/**
 * This function requests the Vault server endpoint to create/update a (new version of a) secret.
 * It manages seamlessly the KV engine version
 * - version 1: https://www.vaultproject.io/api-docs/secret/kv/kv-v1#create-update-secret
 * - version 2: https://www.vaultproject.io/api-docs/secret/kv/kv-v2#create-update-secret
 */
func doUpdateSecret(secretPath string, body string) (map[string]interface{}, error) {
	// 1: determine KV engine version
	if version, err := getKvEngineVersion(secretPath); err != nil {
		return nil, StatusError{
			Code:    http.StatusInternalServerError,
			Message: "Failed retrieving KV engine version",
			Err:     err,
		}
	} else {
		if version == 2 {
			// transform the request path
			secretPath = "data/" + secretPath
			// and also the JSON structure
			body = "{\"data\": " + body + "}"
		}
	}
	url := fmt.Sprintf("%s%s/%s", vaultBaseUrl, vaultBaseSecretsKvPath, secretPath)
	if request, err := http.NewRequest(http.MethodPost, url, bytes.NewBufferString(body)); err != nil {
		// create request error: propagate
		return nil, StatusError{
			Code:    http.StatusInternalServerError,
			Message: "Failed creating request",
			Err:     err,
		}
	} else if token, err := getToken(); err != nil {
		// login error: propagate
		return nil, StatusError{
			Message: "Login failed",
			Err:     err,
		}
	} else {
		request.Header.Set("Content-Type", "application/json")
		request.Header.Set(ClientTokenHeader, token)
		if len(vaultNamespace) > 0 {
			request.Header.Set(NamespaceHeader, vaultNamespace)
		}

		log.Printf("... create secret '%s' (POST %s)\n", secretPath, url)
		client := &http.Client{}
		if resp, err := client.Do(request); err != nil {
			// request error: propagate
			return nil, StatusError{
				Code:    http.StatusInternalServerError,
				Message: "Failed sending request",
				Err:     err,
			}
		} else if resp.StatusCode != http.StatusOK {
			// http error: decode error body and throw error
			defer resp.Body.Close()
			var errResp ErrorResponse
			// nolint
			json.NewDecoder(resp.Body).Decode(&errResp)

			return nil, StatusError{
				Code:    resp.StatusCode,
				Message: fmt.Sprintf("Vault server error on POST %s", url),
				Err:     errResp,
			}
		} else {
			// OK: decode response
			defer resp.Body.Close()
			var secret UpdateSecretResponse
			if err := json.NewDecoder(resp.Body).Decode(&secret); err != nil {
				// JSON unmarshall error: propagate
				return nil, StatusError{
					Code:    http.StatusInternalServerError,
					Message: "Failed unmarshalling response body",
					Err:     err,
				}
			}
			// log.Printf("... secret %s retrieved: %s\n", secretPath, secret.Data)
			return secret.Data, nil
		}
	}
}

/**
 * This function requests the Vault server endpoint to delete the (latest version of the) secret.
 * It manages seamlessly the KV engine version
 * - version 1: https://www.vaultproject.io/api-docs/secret/kv/kv-v1#delete-secret
 * - version 2: https://www.vaultproject.io/api-docs/secret/kv/kv-v2#delete-latest-version-of-secret
 */
func doDeleteSecret(secretPath string) error {
	// 1: determine KV engine version
	if version, err := getKvEngineVersion(secretPath); err != nil {
		return StatusError{
			Code:    http.StatusInternalServerError,
			Message: "Failed retrieving KV engine version",
			Err:     err,
		}
	} else {
		if version == 2 {
			secretPath = "data/" + secretPath
		}
	}
	url := fmt.Sprintf("%s%s/%s", vaultBaseUrl, vaultBaseSecretsKvPath, secretPath)
	if request, err := http.NewRequest(http.MethodDelete, url, nil); err != nil {
		// create request error: propagate
		return StatusError{
			Code:    http.StatusInternalServerError,
			Message: "Failed creating request",
			Err:     err,
		}
	} else if token, err := getToken(); err != nil {
		// login error: propagate
		return StatusError{
			Message: "Login failed",
			Err:     err,
		}
	} else {
		request.Header.Set("Content-Type", "application/json")
		request.Header.Set(ClientTokenHeader, token)
		if len(vaultNamespace) > 0 {
			request.Header.Set(NamespaceHeader, vaultNamespace)
		}

		log.Printf("... delete secret '%s' (DELETE %s)\n", secretPath, url)
		client := &http.Client{}
		if resp, err := client.Do(request); err != nil {
			// request error: propagate
			return StatusError{
				Code:    http.StatusInternalServerError,
				Message: "Failed sending request",
				Err:     err,
			}
		} else if resp.StatusCode != http.StatusNoContent {
			// http error: decode error body and throw error
			defer resp.Body.Close()
			var errResp ErrorResponse
			// nolint
			json.NewDecoder(resp.Body).Decode(&errResp)

			return StatusError{
				Code:    resp.StatusCode,
				Message: fmt.Sprintf("Vault server error on DELETE %s", url),
				Err:     errResp,
			}
		} else {
			return nil
		}
	}
}

/**
 * Retrieves a sub object within a map
 * @arg obj: a map object
 * @arg keyPath: dot separated keys path (ex: '.', 'path.to.field')
 */
func getObj(obj map[string]interface{}, keyPath string, b64Decode bool) (string, error) {
	keys := strings.Split(keyPath, ".")
	var sub interface{}
	sub = obj
	for index := 0; index < len(keys); index++ {
		if len(keys[index]) > 0 {
			// check sub is a map (or fail)
			if asMap, ok := sub.(map[string]interface{}); ok {
				// retrieve sub object
				sub = asMap[keys[index]]
				// check not null
				if sub == nil {
					return "", fmt.Errorf("No such object: '%s'", strings.Join(keys[0:index+1], "."))
				}
			} else {
				return "", fmt.Errorf("Object at '%s' is not a map", strings.Join(keys[0:index+1], "."))
			}
		}
	}
	// return sub as raw string or JSON
	if asStr, ok := sub.(string); !ok {
		if jsonStr, err := json.Marshal(sub); err != nil {
			return "", fmt.Errorf("Failed marshalling object '%s' due to %v", keyPath, err)
		} else {
			return string(jsonStr), nil
		}
	} else {
		if b64Decode {
			data, err := base64.StdEncoding.DecodeString(asStr)
			if err != nil {
				return "", fmt.Errorf("Failed b64decode object '%s' due to %v", keyPath, err)
			} else {
				str := TrimSuffix(string(data), "\n")
				return str, nil
			}
		} else {
			return asStr, nil
		}
	}
}

/**
 * Check json format into request body
 */
func checkBody(request *http.Request) (int, []byte, error) {
	bodyStr, _ := ioutil.ReadAll(request.Body)
	if len(bodyStr) <= 0 {
		return http.StatusBadRequest, nil, fmt.Errorf("request body must not be empty")
	}
	err := json.Unmarshal(bodyStr, &struct{}{})
	if err != nil {
		var syntaxError *json.SyntaxError
		switch {
		// Catch any syntax errors in the JSON and send an error message
		// which interpolates the location of the problem to make it
		// easier for the client to fix.
		case errors.As(err, &syntaxError):
			// http.Error(writer, msg, http.StatusBadRequest)
			return http.StatusBadRequest, nil, fmt.Errorf("request body contains badly-formed json (at position %d)", syntaxError.Offset)

		// In some circumstances Decode() may also return an
		// io.ErrUnexpectedEOF error for syntax errors in the JSON. There
		// is an open issue regarding this at
		// https://github.com/golang/go/issues/25956.
		case errors.Is(err, io.ErrUnexpectedEOF):
			// http.Error(writer, msg, http.StatusBadRequest)
			return http.StatusBadRequest, nil, fmt.Errorf("request body contains badly-formed json")

		// An io.EOF error is returned by Decode() if the request body is
		// empty.
		// case errors.Is(err, io.EOF):
		// 	// http.Error(writer, msg, http.StatusBadRequest)
		// 	return http.StatusBadRequest, nil, fmt.Errorf("request body must not be empty")

		// Catch the error caused by the request body being too large. Again
		// there is an open issue regarding turning this into a sentinel
		// error at https://github.com/golang/go/issues/30715.
		case err.Error() == "http: request body too large":
			// http.Error(writer, msg, http.StatusRequestEntityTooLarge)
			return http.StatusRequestEntityTooLarge, nil, fmt.Errorf("request body must not be larger than 1mb")

		// Otherwise default to logging the error and sending a 500 Internal
		// Server Error response.
		default:
			return http.StatusInternalServerError, nil, fmt.Errorf(err.Error())
			// log.Println(err.Error())
			// http.Error(writer, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		}
	}
	return 200, bodyStr, nil
}

/**
 * This is the /api/secrets API endpoint
 * Routes the incoming request depending on the Http method:
 * - GET /api/secrets/{path}
 * - PUT/POST /api/secrets/{path}
 * - DELETE /api/secrets/{path}
 */
func SecretsEndpoint(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		GetSecret(w, r)
	case "POST", "PUT":
		UpdateSecret(w, r)
	case "DELETE":
		DeleteSecret(w, r)
	default:
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}
}

/**
 * GET /api/secrets endpoint function
 */
func GetSecret(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "text/plain")
	secretPath := request.URL.Path[13:]
	keyPath := request.URL.Query().Get("field")
	b64Decode := request.URL.Query().Has("b64decode")

	if secret, err := getSecret(secretPath, keyPath, b64Decode); err != nil {
		log.Printf("Get secret '%s' (%s) failed: [%d] %s\n", secretPath, keyPath, err.(StatusError).Status(), err.Error())
		http.Error(writer, err.Error(), err.(StatusError).Status())
	} else {
		writer.WriteHeader(http.StatusOK)
		if _, err = writer.Write([]byte(secret)); err != nil {
			log.Println("Error while sending response")
		}
	}
}

/**
 * PUT /api/secrets endpoint function
 */
func UpdateSecret(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")
	secretPath := request.URL.Path[13:]

	if status, body, err := checkBody(request); err != nil {
		log.Printf("%d %s", status, err)
		http.Error(writer, err.Error(), status)
	} else {
		if createResp, err := doUpdateSecret(secretPath, string(body)); err != nil {
			http.Error(writer, err.Error(), err.(StatusError).Status())
		} else {
			delete(path2secret, secretPath)
			createJson, err := json.Marshal(createResp)
			if err != nil {
				http.Error(writer, err.Error(), http.StatusInternalServerError)
				return
			}
			writer.WriteHeader(http.StatusOK)
			if _, err = writer.Write([]byte(createJson)); err != nil {
				log.Println("Error while sending response")
			}
		}
	}
}

/**
 * DELETE /api/secrets endpoint function
 */
func DeleteSecret(writer http.ResponseWriter, request *http.Request) {
	secretPath := request.URL.Path[13:]

	if err := doDeleteSecret(secretPath); err != nil {
		http.Error(writer, err.Error(), err.(StatusError).Status())
	} else {
		delete(path2secret, secretPath)
		writer.WriteHeader(http.StatusNoContent)
	}
}
