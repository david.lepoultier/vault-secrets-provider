{
    "openapi" : "3.0.3",
    "info" : {
      "title" : "Vault Secrets Provider",
      "description" : "This API allows managing secrets from the Vault server.",
      "version" : "1.0"
    },
    "paths" : {
      "/api/secrets/{secret_path}" : {
        "delete": {
          "summary" : "Delete a secret from the Vault server",
          "description": "Deletes the given secret, seamlessly using the right API depending on the detected Key-Value engine version ([version 1](https://www.vaultproject.io/api-docs/secret/kv/kv-v1#delete-secret) or [version 2](https://www.vaultproject.io/api-docs/secret/kv/kv-v2#delete-latest-version-of-secret)).",
          "parameters": [
            {
              "name" : "secret_path",
              "in" : "path",
              "description" : "your secret location in the Vault server",
              "required" : true,
              "schema" : {
                "pattern" : ".+",
                "type" : "string",
                "nullable" : false
              },
              "example" : "b7ecb6ebabc231/my-backend/review"
            }
          ],
          "responses": {
            "204": {
              "description": "OK"
            }
          }
        },
        "get" : {
          "summary" : "Read a secret from the Vault server",
          "description": "Reads the given secret, seamlessly using the right API depending on the detected Key-Value engine version ([version 1](https://www.vaultproject.io/api-docs/secret/kv/kv-v1#read-secret) or [version 2](https://www.vaultproject.io/api-docs/secret/kv/kv-v2#read-secret-version)).",
          "parameters" : [ {
            "name" : "secret_path",
            "in" : "path",
            "description" : "your secret location in the Vault server",
            "required" : true,
            "schema" : {
              "pattern" : ".+",
              "type" : "string",
              "nullable" : false
            },
            "example" : "b7ecb6ebabc231/my-backend/prod"
          }, {
            "name" : "field",
            "in" : "query",
            "description" : "the JSON key path to access a single basic-type field from the JSON payload",
            "required" : true,
            "schema" : {
              "type" : "string"
            },
            "example" : "mysql.password"
          } ],
          "responses" : {
            "400" : {
              "description" : "You're probably misusing the API (trying to access a non-basic type JSON field?)"
            },
            "404" : {
              "description" : "You're probably misusing the API (trying to access a JSON field that doesn't exist in the secret's JSON payload?)"
            },
            "401" : {
              "description" : "You probably didn't configure correctly the Vault credentials"
            },
            "403" : {
              "description" : "You're using an account that doesn't have access privileges to the secret"
            },
            "500" : {
              "description" : "Ouch this is bad! Probably a bug."
            },
            "200" : {
              "description" : "Secret successfully retrieved",
              "content" : {
                "text/plain" : {
                  "schema" : {
                    "type" : "string"
                  }
                }
              }
            }
          }
        },
        "put" : {
          "summary" : "Create/Update a secret into the Vault server",
          "description": "Creates/Updates the given secret, seamlessly using the right API depending on the detected Key-Value engine version ([version 1](https://www.vaultproject.io/api-docs/secret/kv/kv-v1#create-update-secret) or [version 2](https://www.vaultproject.io/api-docs/secret/kv/kv-v2#create-update-secret)).",
          "parameters" : [ 
            {
              "name" : "secret_path",
              "in" : "path",
              "description" : "your secret location in the Vault server",
              "required" : true,
              "schema" : {
                "pattern" : ".+",
                "type" : "string",
                "nullable" : false
              },
              "example" : "b7ecb6ebabc231/my-backend/review"
            } 
          ],
          "requestBody": {
            "content": {
              "application/json": {
                "schema":{
                  "type": "object",
                  "example": {
                    "token": "ŧ0k3N",
                    "secret": "$€cr€T",
                    "mysql": {
                      "user": "root",
                      "password": "p@s5w0rd"
                    }
                  }
                }
              }
            }
          },
          "responses" : {
            "201" : {
              "description" : "Secret successfully updated",
              "content" : {
                "application/json" : {
                  "schema" : {
                    "type": "object"
                  }
                }
              }
            }
          }
        }
      }
    }
  }