<!DOCTYPE html>
<html lang="en">

<head>
    <title>Vault Secrets Provider</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="favicon.png">
    <link rel="stylesheet" href="markdown.css">
</head>

<body>
    <h1>Vault Secrets Provider</h1>
    <p>The service exposes one single API to read, create/update or delete a secret from a <a href="https://www.vaultproject.io/" rel="nofollow noreferrer noopener" target="_blank">Vault</a> server.</p>
    <p>It is smart enough to auto-detect whether the Vault server is configured to use KV Secrets Engine version 1 (unversioned mode) or version 2 (versioned mode).
        Thus you don't need to worry about the <code>data</code> part in the resource path or in the response object structure.</p>

    <h2>Requirements and configuration</h2>
    <h3>Requirements</h3>
    <p>
        Before using this service, you'll have to configure your Vault server, with:

        <ul>
            <li>one or several secrets,</li>
            <li>at least one of the following <a href="https://www.vaultproject.io/docs/auth" rel="nofollow noreferrer noopener" target="_blank">Auth Methods</a> configured with required permissions to access those secrets:
                <ul>
                    <li><a href="https://www.vaultproject.io/docs/auth/approle" rel="nofollow noreferrer noopener" target="_blank">AppRole</a>,</li>
                    <li><a href="https://www.vaultproject.io/docs/auth/token" rel="nofollow noreferrer noopener" target="_blank">Token</a>,</li>
                    <li>or <a href="https://www.vaultproject.io/docs/auth/jwt" rel="nofollow noreferrer noopener" target="_blank">JWT</a>.</li>
                </ul>
        </li>
        </ul>
    </p>
    <h3>Configuration parameters</h3>
    <p>The program supports the following environment variables:</p>
    <table>
        <thead>
            <tr>
                <th>Name</th>
                <th>description</th>
                <th>default value</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><code>VAULT_BASE_URL</code></td>
                <td>The Vault server base API url</td>
                <td><em>none</em></td>
            </tr>
            <tr>
                <td><code>VAULT_NAMESPACE</code></td>
                <td>The Vault <a href="https://www.vaultproject.io/api-docs#namespaces"
                        rel="nofollow noreferrer noopener" target="_blank">Namespace</a> to retrieve secrets into</td>
                <td><em>none</em></td>
            </tr>
            <tr>
                <td><code>VAULT_BASE_AUTH_APPROLE_PATH</code></td>
                <td>The base <a href="https://www.vaultproject.io/api-docs/auth/approle"
                        rel="nofollow noreferrer noopener" target="_blank">AppRole authentication</a> API path</td>
                <td><code>/auth/approle</code></td>
            </tr>
            <tr>
                <td><code>VAULT_BASE_AUTH_JWT_PATH</code></td>
                <td>The base <a href="https://www.vaultproject.io/api-docs/auth/jwt" rel="nofollow noreferrer noopener"
                        target="_blank">JWT/OIDC authentication</a> API path</td>
                <td><code>/auth/jwt</code></td>
            </tr>
            <tr>
                <td><code>VAULT_BASE_KV_SECRETS_PATH</code></td>
                <td>The base <a href="https://www.vaultproject.io/api-docs/secret/kv/kv-v1"
                        rel="nofollow noreferrer noopener" target="_blank">Key/Value secrets</a> API path</td>
                <td><code>/secret</code></td>
            </tr>
            <tr>
                <td><code>VAULT_ROLE_ID</code></td>
                <td>The <a href="https://www.vaultproject.io/docs/auth/approle" rel="nofollow noreferrer noopener"
                        target="_blank">AppRole</a> RoleID
                        <br/><em>Required for the <a href="https://www.vaultproject.io/docs/auth/approle"
                            rel="nofollow noreferrer noopener" target="_blank">AppRole</a> Auth Method</em>
                <td>
                    <em>none</em>
                </td>
            </tr>
            <tr>
                <td><code>VAULT_SECRET_ID</code></td>
                <td>The <a href="https://www.vaultproject.io/docs/auth/approle" rel="nofollow noreferrer noopener"
                        target="_blank">AppRole</a> SecretID
                        <br/><em>Required for the <a href="https://www.vaultproject.io/docs/auth/approle"
                            rel="nofollow noreferrer noopener" target="_blank">AppRole</a> Auth Method</em>
                    </td>
                <td>
                    <em>none</em>
                </td>
            </tr>
            <tr>
                <td><code>VAULT_TOKEN</code></td>
                <td>The authentication token
                        <br/><em>Required for the <a href="https://www.vaultproject.io/docs/auth/token"
                            rel="nofollow noreferrer noopener" target="_blank">Token</a> Auth Method</em>
                    </td>
                <td>
                    <em>none</em>
                </td>
            </tr>
            <tr>
                <td><code>VAULT_JWT_TOKEN</code></td>
                <td>The signed <a href="https://en.wikipedia.org/wiki/JSON_Web_Token" rel="nofollow noreferrer noopener"
                        target="_blank">JSON Web Token</a> to login
                        <br/><em>Required for the <a href="https://www.vaultproject.io/docs/auth/jwt"
                            rel="nofollow noreferrer noopener" target="_blank">JWT/OIDC</a> Auth Method</em>
                    </td>
                <td>
                    <code>$CI_JOB_JWT</code>
                </td>
            </tr>
            <tr>
                <td><code>VAULT_JWT_ROLE</code></td>
                <td>Name of the role against which the login is being attempted
                    <br/><em>Required for the <a href="https://www.vaultproject.io/docs/auth/jwt"
                        rel="nofollow noreferrer noopener" target="_blank">JWT/OIDC</a> Auth Method</em>
                </td>
                <td>
                    <code>default_role</code>
                </td>
            </tr>
        </tbody>
    </table>
    <p>If no authentication parameter is set, the image will emit an error log at startup.</p>

    <h2>API reference</h2>
    <h3>&gt; GET secret</h3>
    <p>Read a secret from the Vault server, seamlessly using the right API depending on the detected Key-Value engine version (<a rel="nofollow noreferrer noopener" target="_blank" href="https://www.vaultproject.io/api-docs/secret/kv/kv-v1#read-secret">version 1</a> or <a rel="nofollow noreferrer noopener" target="_blank" href="https://www.vaultproject.io/api-docs/secret/kv/kv-v2#read-secret-version">version 2</a>).</p>
    <pre class="code highlight js-syntax-highlight plaintext white" lang="plaintext"
        v-pre="true"><code><span id="LC1" class="line" lang="plaintext">GET /api​/secrets​/{secret_path}</span></code></pre>
    <h4>Parameters</h4>
    <table>
        <thead>
            <tr>
                <th>Name</th>
                <th>description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <code>secret_path</code> (<em>path parameter</em>)
                </td>
                <td>this is your secret location in the Vault server</td>
            </tr>
            <tr>
                <td>
                    <code>field</code> (<em>query parameter</em>)
                </td>
                <td>parameter to access a single basic field from the secret JSON payload</td>
            </tr>
            <tr>
                <td>
                    <code>b64decode</code> (<em>query parameter</em>)
                </td>
                <td>paramater to decode a single basic field in b64 from the secret</td>
            </tr>
        </tbody>
    </table>
    <h4>Example</h4>
    <p>Let's suppose your have a secret stored under <code>/b7ecb6ebabc231/my-backend/prod</code> location in Vault with
        JSON payload:</p>
    <pre><code>{
  "token": "ŧ0k3N",
  "secret": "$€cr€T",
  "mysql": {
    "user": "root",
    "password": "p@s5w0rd"
  },
  "config": "ewogICJraW5kIjogIkNvbmZpZyIsCiAgImFwaVZlcnNpb24iOiAidjEiLAogICJwcmVmZXJlbmNlcyI6IHt9LAp9Cg=="
}</code></pre>
    <p>Then you may retrieve:</p>
    <ul>
        <li>the token calling
            <code>GET http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/my-backend/prod?field=token</code>
        </li>
        <li>the MySql password calling
            <code>GET http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/my-backend/prod?field=mysql.password</code>
        </li>
        <li>the config b64 decoded calling
            <code>GET http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/my-backend/prod?field=config&b64decode</code>
        </li>
    </ul>
    
    <h3>&gt; PUT secret</h3>
    <p>Create/Update a secret into the Vault server, seamlessly using the right API depending on the detected Key-Value engine version (<a rel="nofollow noreferrer noopener" target="_blank" href="https://www.vaultproject.io/api-docs/secret/kv/kv-v1#create-update-secret">version 1</a> or <a rel="nofollow noreferrer noopener" target="_blank" href="https://www.vaultproject.io/api-docs/secret/kv/kv-v2#create-update-secret">version 2</a>).</p>
    <pre class="code highlight js-syntax-highlight plaintext white" lang="plaintext"
        v-pre="true"><code><span id="LC1" class="line" lang="plaintext">POST /api​/secrets​/{secret_path} \
    -d '{"token": "@n0ther_ŧ0k3N", "secret": "n€w_$€cr€T"}' 
    -H "Content-Type: application/json"</span></code></pre>
    <p>Depending on the Key-Value engine version the Vault server is using, this might either overwrite the secret (v1) or create a new version (v2).
        When using version 2, you don't need to embed your secret payload into a `data` field - this is done automatically for you.</p>
    <h4>Parameters</h4>
    <table>
        <thead>
            <tr>
                <th>Name</th>
                <th>description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <code>secret_path</code> (<em>path parameter</em>)
                </td>
                <td>this is your secret location in the Vault server</td>
            </tr>
        </tbody>
    </table>
    <h4>Example</h4>
    <p>Create/update a secret under <code>/b7ecb6ebabc231/my-backend/review</code> location in Vault with
        JSON payload:</p>
    <pre><code>curl -X PUT http://localhost:8080/api/secret/b7ecb6ebabc231/my-backend/review \
        -d '{"token": "@n0ther_ŧ0k3N", "secret": "n€w_$€cr€T"}' \
        -H "Content-Type: application/json"
    </code></pre>
    <pre><code>{
    "created_time":"2021-11-10T10:40:49.286084835Z",
    "deletion_time":"",
    "destroyed":false,
    "version":1
}</code></pre>

    <h3>&gt; DELETE secret</h3>
    <p>Delete a secret from the Vault server, seamlessly using the right API depending on the detected Key-Value engine version (<a rel="nofollow noreferrer noopener" target="_blank" href="https://www.vaultproject.io/api-docs/secret/kv/kv-v1#delete-secret">version 1</a> or <a rel="nofollow noreferrer noopener" target="_blank" href="https://www.vaultproject.io/api-docs/secret/kv/kv-v2#delete-latest-version-of-secret">version 2</a>).</p>
    <pre class="code highlight js-syntax-highlight plaintext white" lang="plaintext"
        v-pre="true"><code><span id="LC1" class="line" lang="plaintext">DELETE /api​/secrets​/{secret_path}</span></code></pre>
    <h4>Parameters</h4>
    <table>
        <thead>
            <tr>
                <th>Name</th>
                <th>description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <code>secret_path</code> (<em>path parameter</em>)
                </td>
                <td>this is your secret location in the Vault server</td>
            </tr>
        </tbody>
    </table>
    <h4>Example</h4>
    <p>DELETE a secret stored under <code>/b7ecb6ebabc231/my-backend/review</code> location in Vault with
        JSON payload:</p>
    <pre><code>curl -X DELETE http://localhost:8080/api/secret/b7ecb6ebabc231/my-backend/review</code></pre>
    
    <!--
    <h4>
        <a id="user-content-use-in-gitlab-ci" class="anchor" href="#use-in-gitlab-ci" aria-hidden="true"></a>Use in
        GitLab CI
    </h4>
    <p>Finally, the Docker image can be used in your GitLab CI files as follows:</p>
    <pre class="code highlight js-syntax-highlight yaml white" lang="yaml"
        v-pre="true"><code><span id="LC1" class="line" lang="yaml"><span class="na">variables</span><span class="pi">:</span></span>
        <span id="LC2" class="line" lang="yaml">  <span class="c1"># variables have to be explicitly declared in the YAML to be exported to the service</span></span>
        <span id="LC3" class="line" lang="yaml">  <span class="na">VAULT_BASE_URL</span><span class="pi">:</span> <span class="s2">"</span><span class="s">https://vault.secrets.acme.host/v1"</span></span>
        <span id="LC4" class="line" lang="yaml">  <span class="na">VAULT_ROLE_ID</span><span class="pi">:</span> <span class="s2">"</span><span class="s">$VAULT_ROLE_ID"</span></span>
        <span id="LC5" class="line" lang="yaml">  <span class="na">VAULT_SECRET_ID</span><span class="pi">:</span> <span class="s2">"</span><span class="s">$VAULT_SECRET_ID"</span></span>
        <span id="LC6" class="line" lang="yaml"></span>
        <span id="LC7" class="line" lang="yaml"><span class="na">deploy-job</span><span class="pi">:</span></span>
        <span id="LC8" class="line" lang="yaml">  <span class="na">image</span><span class="pi">:</span> <span class="s">my-deploy-tool:latest</span></span>
        <span id="LC9" class="line" lang="yaml">  <span class="na">services</span><span class="pi">:</span> </span>
        <span id="LC10" class="line" lang="yaml">    <span class="c1"># add Vault Secrets Provider as a service</span></span>
        <span id="LC11" class="line" lang="yaml">    <span class="c1"># requires that VAULT_ROLE_ID and VAULT_SECRET_ID are declared as secret variables</span></span>
        <span id="LC12" class="line" lang="yaml">    <span class="pi">-</span> <span class="na">name</span><span class="pi">:</span> <span class="s">vault-secrets-provider:master</span></span>
        <span id="LC13" class="line" lang="yaml">      <span class="na">alias</span><span class="pi">:</span> <span class="s">vault-secrets-provider</span></span>
        <span id="LC14" class="line" lang="yaml">  <span class="na">before-script</span><span class="pi">:</span></span>
        <span id="LC15" class="line" lang="yaml">    <span class="c1"># retrieve some token from Vault server</span></span>
        <span id="LC16" class="line" lang="yaml">    <span class="pi">-</span> <span class="s">my_token=$(curl -s -S -f "http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/my-backend/prod?field=token")</span></span>
        <span id="LC17" class="line" lang="yaml">    <span class="c1"># then login</span></span>
        <span id="LC18" class="line" lang="yaml">    <span class="pi">-</span> <span class="s">my-deploy-tool login --token $my_token</span></span>
        <span id="LC19" class="line" lang="yaml">  <span class="na">script</span><span class="pi">:</span></span>
        <span id="LC20" class="line" lang="yaml">    <span class="c1"># deploy (pseudo code)</span></span>
        <span id="LC21" class="line" lang="yaml">    <span class="pi">-</span> <span class="s">my-deploy-tool deploy --other --args</span></span></code></pre>
    <p>Depending on what is available in your docker image, you may request the service using either <code>curl</code>
        or <code>wget</code>:</p>
    <ul>
        <li><code>curl -s -S -f "http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/my-backend/prod?field=token"</code>
        </li>
        <li><code>wget -O - "http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/my-backend/prod?field=token"</code>
        </li>
    </ul>
    -->
    <footer>
        <a href="/openapi.json">Open API specs</a>
    </footer>
</body>

</html>