module tbc.orange.com/vault-secrets-provider

go 1.13

require (
	golang.org/x/tools/gopls v0.6.9 // indirect
	google.golang.org/grpc v1.43.0
)
